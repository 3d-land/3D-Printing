# Socket ESP8266 (ESP12E)


## Images
![kicad](/ESP12E_Programing/PCB/kicad.png)
![pcb](/ESP12E_Programing/PCB/PCB/Socket_ESP12E-B.Cu.pdf)

## PCB
[PCB (PDF)](/ESP12E_Programing/PCB/PCB/Socket_ESP12E-B.Cu.pdf)
[PCB (Gerber)](/ESP12E_Programing/PCB/PCB/)

## License
Copyright 2018, S. Fabian (fAnDREs)
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)


## Attributions
### Library kicad (Schematic)
https://github.com/jdunmire/kicad-ESP8266
Copyright 2015,2016 J.Dunmire
Contact: jedunmire PLUS kicad-ESP8266 AT gmail DOT com
This file is part of kicad-ESP8266.
kicad-ESP8266 is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license visit http://creativecommons.org/licenses/by-sa/4.0/.


### See commit details to find the authors of each Part.
- @fandres323
