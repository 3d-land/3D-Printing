# Socket ESP8266 (ESP12E)


## Images
![Print](/ESP12E_Programing/Socket/Print.jpg)
![3D_1](/ESP12E_Programing/Socket/3D_2.png)
![3D_2](/ESP12E_Programing/Socket/3D_3.png)

NT:In the next few weeks design the PCB to support the socket

## License
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/)

## Printing parameters
Print Settings

- Printer Brand: Prusa
- Printer: Prusa Clone
- Rafts: No
- Supports: No
- Resolution: Adaptative Quality
- Infill:35%


Notes:

- First layer Height: 0.18

## Post-Printing

Internal Drills
Adjust internal holes with motortool.

## Attributions
Based on the work of [WDWHITE ](https://www.thingiverse.com/thing:1333056)
Step File by by_Andrey_Chirva
See commit details to find the authors of each Part.
- @fandres323
