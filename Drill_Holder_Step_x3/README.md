﻿# Drill_Holder for Step Drill Bits 3-12mm 4-12mm 4-20mm HSS


## Properties
- Modeling in FreeCAD
- Fill Density: 10%
- Top Solid Lyers: 4
- Step Drill Bits 3-12mm 4-12mm 4-20mm HSS : [Aliexpress](https://es.aliexpress.com/item/3PCS-High-Speed-Steel-Titanium-Step-Drill-Bits-3-12mm-4-12mm-4-20mm-HSS-Wood/32815211512.html?spm=a219c.10010108.1000016.1.5bb54e59t2Lwzb)

## Images
![freeCAD rendering](/Drill_Holder_Step_x3/Drill_Holder.png)
![Photo](/Drill_Holder_Step_x3/Drill_Holder.jpg)


## License
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)


## Attributions

See commit details to find the authors of each Part.
- @fandres7_7

## See more in the repository
[Gitlab](https://gitlab.com/fandres323/3D-Printing)

