﻿# Cutting guide for 3/4 inch aluminum profile


Modeling in FreeCAD


## Images
![freeCAD rendering](/Cut_guide_0.25Inch/Cut_guide_0.25Inch.png)


## License
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)


## Attributions

See commit details to find the authors of each Part.
- @fandres7_7

