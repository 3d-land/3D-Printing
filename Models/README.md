﻿# Model 3D on FreeCad
============

# Mechanical Parts
- Motors
![3D_Step-syn](Models/Mechanical Parts/Motors/Step-syn.png)

# License 
======

[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

# Attributions
============
See commit details to find the authors of each Part.
- @fandres7_7
