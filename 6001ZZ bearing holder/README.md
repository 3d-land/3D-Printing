﻿# 6001ZZ bearing holder


Modeling in FreeCAD


## Images
![6001ZZ bearing holder 1](/6001ZZ bearing holder/6001ZZ bearing holder.png)
![6001ZZ bearing holder 1](/6001ZZ bearing holder/6001ZZ bearing holder.jpg)

# Dimensions
[Dimesiones PDF](/6001ZZ bearing holder/6001ZZ bearing holder - cotas.pdf)

## License
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)


## Attributions

See commit details to find the authors of each Part.
- @fandres7_7
- 6001ZZ step file by [Tony](https://grabcad.com/library/12mm-bearing-skf-6001-rs-334-1528)
