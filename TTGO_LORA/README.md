﻿# Case for TTGO ESP32

- Modeling in FreeCAD


## Images
![TTGO_LORA_CASE_1](/TTGO_LORA/TTGO_LORA_CASE.jpg)
![TTGO_LORA_CASE_2](/TTGO_LORA/TTGO_LORA_CASE.png)

## License
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)


## Attributions
See commit details to find the authors of each Part.
- @fandres7_7
