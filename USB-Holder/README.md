﻿# Model 3D on FreeCad
============

# Images
![3D_USB-Holder_logo_1](/USB-Holder/USB-Holder_logo_1.png)

# License 
======

[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

# Attributions
============
See commit details to find the authors of each Part.
- @fandres7_7
