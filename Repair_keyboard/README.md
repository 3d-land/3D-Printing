﻿# hex shank tap drill bit holder
- Hex Shank Titanium Plated HSS Screw Thread Metric Tap Drill Bits S08 : [Aliexpress](https://es.aliexpress.com/item/32748148680.html?spm=a2g0s.9042311.0.0.274263c01lcQM2)


Modeling in FreeCAD


## Images
![Drill Holder Hex S08 1](/Repair_keyboard/keyboard_repair_2.jpg)
![Drill Holder Hex S08 1](/Repair_keyboard/keyboard_repair_1.jpg)



## License
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)


## Attributions

See commit details to find the authors of each Part.
- @fandres7_7

## See more in the repository
[Gitlab](https://gitlab.com/3d-land/3D-Printing)
