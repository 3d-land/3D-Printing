﻿# Sure Garden


## Properties
- Modeling in FreeCAD


## Images
![Photo secure](/huerta/huerta1.png)
![Photo secure](/huerta/huerta2.png)


## License
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)


## Attributions

See commit details to find the authors of each Part.
- @fandres7_7

## See more in the repository
[Gitlab](https://gitlab.com/fandres323/3D-Printing)

