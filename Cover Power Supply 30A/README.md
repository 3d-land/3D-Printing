﻿# Cover Power Supply 30A on FreeCad

## Properties
- Modeling in FreeCAD
- Suport Material: yes
- Overlang threshould: 5
- Switch Male Power Socket : [Aliexpress](https://es.aliexpress.com/item/Hot-10A-250V-Inlet-Module-Plug-Fuse-Switch-Male-Power-Socket-3-Pin-IEC320-C14-New/32829721845.html?spm=a219c.10010108.1000016.1.52304e32vVDXOq&isOrigTitle=true)

## Images
![3D](/Cover Power Supply 30A Photo/Cover_Power_Supply_30A.jpg)
![3D](/Cover Power Supply 30A/Cover_Power_Supply_30A_3D.png)
![Slic3r](/Cover Power Supply 30A/Cover_Power_Supply_30A_slic3r.png)

## License

[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)


## Attributions
See commit details to find the authors of each Part.
- @fandres7_7

## See more in the repository
[Gitlab](https://gitlab.com/fandres323/3D-Printing)
